#!/bin/sh

BUILDBOX=${BUILDBOX:-yukoff/slackware-buildbox-32bit:14.2}

#TEMPVOL=/var/tmp/docker-builder-$$
TEMPVOL=/var/tmp/docker-builder-last
mkdir -p $TEMPVOL
echo Using shared TMP=$TEMPVOL

docker run -it --rm \
           -v $(pwd):/kbuild \
           -v $TEMPVOL:/tmp \
           $BUILDBOX -x /kbuild/build-armored-kernel.sh

