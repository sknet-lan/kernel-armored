#!/bin/bash
# File to update source-timestamp

cd $(dirname $0) ; CWD=$(pwd)

GIT_DIR=${GIT_DIR:-/usr/src/linux?git/.git}

echo -e $(TZ=UTC git show --quiet --date=iso-local v4.4.172 --format="%cd\nGIT Revision: %H\nGIT Branch: stable" | tail -1)
