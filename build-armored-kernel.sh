#!/bin/sh

# This one is based on original build-all-kernels.sh
#
# Copyright 2018  Patrick J. Volkerding, Sebeka, Minnesota, USA
# Copyright 2019  yukoff, Chernihiv, Ukraine
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# This script uses the SlackBuild scripts present here to build a
# complete set of kernel packages for the currently running architecture.
# It needs to be run once on 64-bit (uname -m = x86_64) and once on IA32
# (uname -m = i586 or i686).

set -e

cd $(dirname $0) ; CWD=$(pwd)

CPUCORES=$(cat /proc/cpuinfo | grep 'cpu cores' | cut -d: -f2)
NUMJOBS=${NUMJOBS:-" -j${CPUCORES} "}

# some workarounds, this should be moved to Dockerfile
slackpkg -batch=on -default_answer=y update
slackpkg -batch=on -default_answer=y install \
    kernel-headers kmod less libarchive nettle lzo lz4 \
    libxml2 guile gc libffi libmpc iptables glibc dev86
slackpkg -batch=on -default_answer=y upgrade-all
# rm -rf /var/cache/packages/*
# rm -rf /var/lib/slackpkg/*
# rm -rf /var/log/removed_packages/*
# rm -rf /var/log/removed_scripts/*
# rm -rf /usr/share/locale/*
# rm -rf /usr/man/*

BUILD=${BUILD:-1}
if [ -z "$VERSION" ]; then
  # Get $VERSION from the newest kernel tarball:
  VERSION=${VERSION:-$(/bin/ls -t linux-*.tar.?z | head -n 1 | rev | cut -f 3- -d . | cut -f 1 -d - | rev)}
fi
TMP=${TMP:-/tmp}

# By default, do NOT install the packages as we might be building inside docker.
INSTALL_PACKAGES=${INSTALL_PACKAGES:-NO}
#unset INSTALL_PACKAGES

# A list of recipes for build may be passed in the $RECIPES variable, otherwise
# we have defaults based on uname -m:
if [ -z "$RECIPES" ]; then
  if uname -m | grep -wq x86_64 ; then
    RECIPES="x86_64_ARMORED"
  elif uname -m | grep -wq i.86 ; then
    #RECIPES="IA32_NO_SMP IA32_SMP"
    RECIPES="IA32_ARMORED" #SMP
  else
    echo "Error: no build recipes available for $(uname -m)"
    exit 1
  fi
fi

# Main build loop:
for recipe in $RECIPES ; do

  # Build recipes are defined here. These will select the appropriate .config
  # files and package naming scheme, and define the output location.
  # We building armored kernel by default, otherwise `unset LOCALVERSION`
  LOCALVERSION="-armored"
  ARMORED=YES
  if [ "$recipe" = "x86_64_ARMORED" ]; then
    # Recipe for x86_64_ARMORED:
    CONFIG_SUFFIX=".x64"
    LOCALVERSION="-armored"
    OUTPUT=${OUTPUT:-${TMP}/output-x86_64-${VERSION}}
  elif [ "$recipe" = "x86_64" ]; then
    # Recipe for x86_64:
    CONFIG_SUFFIX=".x64"
    unset LOCALVERSION
    unset ARMORED
    OUTPUT=${OUTPUT:-${TMP}/output-x86_64-${VERSION}}
  elif [ "$recipe" = "IA32_ARMORED" ]; then
    # Recipe for IA32_ARMORED:
    unset CONFIG_SUFFIX
    LOCALVERSION="-armored"
    OUTPUT=${OUTPUT:-${TMP}/output-ia32-${VERSION}}
  elif [ "$recipe" = "IA32_SMP" ]; then
    # Recipe for IA32_SMP:
    unset CONFIG_SUFFIX
    LOCALVERSION="-smp"
    unset ARMORED
    OUTPUT=${OUTPUT:-${TMP}/output-ia32-${VERSION}}
  elif [ "$recipe" = "IA32_NO_SMP" ]; then
    # Recipe for IA32_NO_SMP:
    unset CONFIG_SUFFIX
    unset LOCALVERSION
    unset ARMORED
    OUTPUT=${OUTPUT:-${TMP}/output-ia32-${VERSION}}
  else
    echo "Error: recipe ${recipe} not implemented"
    exit 1
  fi

  echo
  echo "*************************************************"
  echo "* Building kernels for recipe ${recipe}..."
  echo "*************************************************"
  echo
  sleep 3

  # set attributes
  chmod +x $CWD/{guards,apply-patches,compute-PATCHVERSION.sh,*.SlackBuild}

  # unpack everything
  if [ ! -d $CWD/patches.apparmor -a ! -e $CWD/patches.apparmor.tar.?z* ]; then
    echo "ERROR: Missing apparmor patches!"
    exit 1
  elif [ ! -d $CWD/patches.apparmor ]; then
    tar -C $CWD -xf $CWD/patches.apparmor.tar.?z*
  fi
  # if [ ! -d $CWD/patches.kernel.org -a -e $CWD/patches.kernel.org.tar.?z* ]; then
  #   tar -C $CWD -xf $CWD/patches.kernel.org.tar.?z*
  # fi
  if [ ! -d $CWD/kernel-configs -a ! -e $CWD/kernel-configs.?z* ]; then
    echo "ERROR: Missing kernel configs!"
    exit 1
  elif [ ! -d $CWD/kernel-configs ]; then
    tar -C $CWD -xf $CWD/kernel-configs.tar.?z*
  fi

  # Override the timestamp 'uname -v' reports with the source timestamp and
  # the commit hash.
  date=$(head -n 1 $CWD/source-timestamp)
  commit=$(sed -n 's/GIT Revision: //p' $CWD/source-timestamp)
  cat > $CWD/.kernel-binary.spec.buildenv <<EOF
export KBUILD_BUILD_TIMESTAMP="$(LANG=C date -d "$date") (${commit:0:7})"
export KBUILD_VERBOSE=0
export KBUILD_SYMTYPES=1
export KBUILD_OVERRIDE=1
export KBUILD_BUILD_USER=geeko
export KBUILD_BUILD_HOST=buildhost
#export HOST_EXTRACFLAGS="-include $CWD/host-memcpy-hack.h"

#KERNEL_NAME=generic-armored
#KERNEL_SOURCE=/usr/src/linux-${patchversion}
#KERNEL_CONFIG=./config-x86/config-generic-armored-%{patchversion}-armored
EOF
  source $CWD/.kernel-binary.spec.buildenv

  # Build kernel-source package:
  KERNEL_SOURCE_PACKAGE_NAME=$(PRINT_PACKAGE_NAME=YES KERNEL_CONFIG="config-generic${LOCALVERSION}-${VERSION}${LOCALVERSION}${CONFIG_SUFFIX}" VERSION=$VERSION BUILD=$BUILD ./kernel-source-armored.SlackBuild)
  KERNEL_CONFIG="config-generic${LOCALVERSION}-${VERSION}${LOCALVERSION}${CONFIG_SUFFIX}" VERSION=$VERSION BUILD=$BUILD ./kernel-source-armored.SlackBuild
  mkdir -p $OUTPUT
  mv ${TMP}/${KERNEL_SOURCE_PACKAGE_NAME} $OUTPUT || exit 1
  if [ "${INSTALL_PACKAGES}" = "YES" ]; then
    installpkg ${OUTPUT}/${KERNEL_SOURCE_PACKAGE_NAME} || exit 1
  fi

  # Build kernel-huge package:
  # We will build in the just-built kernel tree. First, let's put back the
  # symlinks:
  ( cd $TMP/package-kernel-source
    sh install/doinst.sh
  )

  # Build kernel-generic package:
  KERNEL_GENERIC_PACKAGE_NAME=$(PRINT_PACKAGE_NAME=YES KERNEL_NAME=generic KERNEL_SOURCE=$TMP/package-kernel-source/usr/src/linux KERNEL_CONFIG=./kernel-configs/config-generic${LOCALVERSION}-${VERSION}${LOCALVERSION}${CONFIG_SUFFIX} CONFIG_SUFFIX=${CONFIG_SUFFIX} KERNEL_OUTPUT_DIRECTORY=$OUTPUT/kernels/generic$(echo ${LOCALVERSION} | tr -d -).s BUILD=$BUILD ./kernel-generic-armored.SlackBuild)
  KERNEL_NAME=generic KERNEL_SOURCE=$TMP/package-kernel-source/usr/src/linux KERNEL_CONFIG=./kernel-configs/config-generic${LOCALVERSION}-${VERSION}${LOCALVERSION}${CONFIG_SUFFIX} CONFIG_SUFFIX=${CONFIG_SUFFIX} KERNEL_OUTPUT_DIRECTORY=$OUTPUT/kernels/generic$(echo ${LOCALVERSION} | tr -d -).s BUILD=$BUILD ./kernel-generic-armored.SlackBuild
  mv ${TMP}/${KERNEL_GENERIC_PACKAGE_NAME} $OUTPUT || exit 1
  if [ "${INSTALL_PACKAGES}" = "YES" ]; then
    installpkg ${OUTPUT}/${KERNEL_GENERIC_PACKAGE_NAME} || exit 1
  fi

  # Build kernel-modules (for the just built generic kernel, but most of them
  # will also work with the huge kernel):
  KERNEL_MODULES_PACKAGE_NAME=$(PRINT_PACKAGE_NAME=YES KERNEL_SOURCE=$TMP/package-kernel-source/usr/src/linux KERNEL_CONFIG=$TMP/package-kernel-source/usr/src/linux/.config BUILD=$BUILD ./kernel-modules-armored.SlackBuild)
  KERNEL_SOURCE=$TMP/package-kernel-source/usr/src/linux KERNEL_CONFIG=$TMP/package-kernel-source/usr/src/linux/.config BUILD=$BUILD ./kernel-modules-armored.SlackBuild
  mv ${TMP}/${KERNEL_MODULES_PACKAGE_NAME} $OUTPUT || exit 1
  if [ "${INSTALL_PACKAGES}" = "YES" ]; then
    installpkg ${OUTPUT}/${KERNEL_MODULES_PACKAGE_NAME} || exit 1
  fi

  # Build kernel-headers:
  KERNEL_HEADERS_PACKAGE_NAME=$(PRINT_PACKAGE_NAME=YES KERNEL_SOURCE=$TMP/package-kernel-source/usr/src/linux BUILD=$BUILD ./kernel-headers-armored.SlackBuild)
  KERNEL_SOURCE=$TMP/package-kernel-source/usr/src/linux BUILD=$BUILD ./kernel-headers-armored.SlackBuild
  mv ${TMP}/${KERNEL_HEADERS_PACKAGE_NAME} $OUTPUT || exit 1
  if [ "${INSTALL_PACKAGES}" = "YES" ]; then
    upgradepkg --reinstall --install-new ${OUTPUT}/${KERNEL_HEADERS_PACKAGE_NAME} || exit 1
  fi

  # Update initrd:
  if [ "${INSTALL_PACKAGES}" = "YES" ]; then
    # We should already have this...
    #LOCALVERSION="$(cat $TMP/package-kernel-source/usr/src/linux/.config 2> /dev/null | grep CONFIG_LOCALVERSION= | cut -f 2 -d = | tr -d \")"
    if [ -r /etc/mkinitrd.conf ]; then
      mkinitrd -F /etc/mkinitrd.conf -k ${VERSION}${LOCALVERSION}
    else # try this?
      sh /usr/share/mkinitrd/mkinitrd_command_generator.sh -k ${VERSION}${LOCALVERSION} | sed "s/-c -k/-k/g" | bash
    fi
  fi

  echo
  echo "${recipe} kernel packages done!"
  echo

done
